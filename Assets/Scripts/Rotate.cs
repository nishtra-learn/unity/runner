﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    public Vector3 axis = Vector3.forward;
    public float degressPerSecond = 45;
    public bool globalSpace = false;

    private void Update() {
        var relativeTo = globalSpace ? Space.World : Space.Self;
        var angle = degressPerSecond * Time.deltaTime;
        transform.Rotate(axis, angle, relativeTo);
    }
}
