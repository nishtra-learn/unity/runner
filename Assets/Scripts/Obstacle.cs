﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public Camera mainCamera;
    public event Action OnPassedWithoutCollision;
    
    private bool collidedWithPlayer = false;
    private bool hasPassedPlayer = false;

    private void Update()
    {
        if (!hasPassedPlayer && gameObject.transform.position.z < mainCamera.transform.position.z) {
            if (!collidedWithPlayer) {
                OnPassedWithoutCollision?.Invoke();
            }
            hasPassedPlayer = true;
            Destroy(gameObject, 1);
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (other.GetComponent<Player>()) {
            collidedWithPlayer = true;
        }
    }
}
