﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    #region Editor fields
    [SerializeField]
    private int hp = 5;
    [SerializeField]
    private float score = 0;
    [SerializeField]
    private Material defaultMaterial;
    [SerializeField]
    private Material grazeMaterial;
    [SerializeField]
    private ParticleSystem deathParticles;
    #endregion
    

    #region Public interface
    public int HP
    {
        get { return hp; }
        set
        {
            int delta = value - hp;
            
            if (delta < 0 && IsInvincible)
                return;
            
            hp = value;
            OnHealthChange?.Invoke(HP, delta);
        }
    }
    public float Score
    {
        get { return score; }
        set
        {
            float delta = value - score;
            score = value;
            OnScoreChange?.Invoke(Score, delta);
        }
    }
    public bool IsInvincible { get; set; }
    public bool isActive = true;
    public event Action OnDeath;
    public event Action<int, int> OnHealthChange;
    public event Action<float, float> OnScoreChange;
    public event Action<string> OnAnimationStart;
    public event Action<string> OnAnimationEnd;
    #endregion


    private bool collidedWithObstacle = false;
    private float grazeMultiplier = 1f;
    private CharacterController characterController;
    //private new Renderer renderer;
    private new SkinnedMeshRenderer renderer;
    private Animator animator;


    private void Start()
    {
        characterController = GetComponent<CharacterController>();
        //renderer = GetComponent<Renderer>();
        renderer = GetComponentInChildren<SkinnedMeshRenderer>();
        animator = GetComponentInChildren<Animator>();
        
        var victoryAnimationStateBehaviour = animator.GetBehaviour<VictoryAnimation>();
        victoryAnimationStateBehaviour.OnAnimationStart += () => {
            IsInvincible = true;
            OnAnimationStart?.Invoke("victory");
        };
        victoryAnimationStateBehaviour.OnAnimationEnd += () => {
            IsInvincible = false;
            OnAnimationEnd?.Invoke("victory");
        };
    }

    private void Update()
    {
        if (!isActive)
            return;

        Score += Time.deltaTime * 10 * grazeMultiplier;
        if (grazeMultiplier > 1) {
            renderer.material = grazeMaterial;
        }
        else {
            renderer.material = defaultMaterial;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!isActive)
            return;

        if (other.GetComponent<Obstacle>())
        {
            HP--;
            collidedWithObstacle = true;
            if (HP <= 0)
            {
                Die();
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (!isActive)
            return;
        
        if (other.gameObject.GetComponent<ObstacleGrazeCollider>())
        {
            if (collidedWithObstacle)
                return;

            var obstacle = other.gameObject.GetComponentInParent<Obstacle>();
            var obstacleCollider = obstacle.GetComponent<Collider>();
            var closestObstacleSurfacePoint = obstacleCollider.bounds.ClosestPoint(transform.position);
            var closestPlayerSurfacePoint = characterController.bounds.ClosestPoint(obstacleCollider.transform.position);

            if (!obstacleCollider.bounds.Contains(closestPlayerSurfacePoint))
            {
                var thisGrazeMult = 5f;

                if (thisGrazeMult > grazeMultiplier)
                    grazeMultiplier = thisGrazeMult;
            }

            //Debug.DrawLine(transform.position, obstacleCollider.transform.position, Color.yellow);
            //Debug.DrawLine(closestObstacleSurfacePoint, closestPlayerSurfacePoint, Color.magenta);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Obstacle>()) {
            collidedWithObstacle = false;
        }
        else if (other.GetComponent<ObstacleGrazeCollider>())
        {
            grazeMultiplier = 1f;
        }
    }

    private void Die()
    {
        OnDeath?.Invoke();
        isActive = false;
        Instantiate(deathParticles, transform.position, Quaternion.identity);
        gameObject.SetActive(false);
    }

    public void PlayVictoryAnimation() {
        animator.SetTrigger("victory");
    }
}
