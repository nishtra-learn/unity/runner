using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 20f;
    public float strafeSpeed = 20f;
    public float jumpSpeed = 1f;
    public float gravity = 9.8f;

    private CharacterController cc;
    private Animator animator;
    private PlayerSndFx sndFx;
    private float yVelocity;

    private void Awake() {
        cc = GetComponentInParent<CharacterController>();
        animator = GetComponentInChildren<Animator>();
        sndFx = GetComponent<PlayerSndFx>();
    }

    void Update()
    {
        var input = Input.GetAxisRaw("Horizontal");
        var velocity = new Vector3(strafeSpeed * input, 0, speed);
        yVelocity -= gravity * Time.deltaTime;
        // if (cc.isGrounded && yVelocity < 0) {
        //     yVelocity = 0;
        // }
        if (Input.GetKeyDown(KeyCode.Space) && cc.isGrounded) {
            yVelocity = Mathf.Sqrt(jumpSpeed * 3.0f * gravity);
            animator.SetTrigger("jump");
            sndFx.PlayJumpSound();
        }
        velocity.y = yVelocity;
                
        cc.Move(velocity * Time.deltaTime);
        cc.transform.position = new Vector3(
            Mathf.Clamp(cc.transform.position.x, -4.5f, 4.5f), 
            cc.transform.position.y, 
            cc.transform.position.z
        );
    }

    private IEnumerator PlayFootstepSound() {
        yield return new WaitForSeconds(1f);
    }
}
