﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroy : MonoBehaviour
{
    public float timeDelaySeconds = 1;

    private void Start() {
        Destroy(gameObject, timeDelaySeconds);
    }
}
