﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Sound Bank")]
public class SoundBank : ScriptableObject
{
    public Sound[] sounds = new Sound[0];
}
