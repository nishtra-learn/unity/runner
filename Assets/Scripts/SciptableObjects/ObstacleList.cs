﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Obstacle List")]
public class ObstacleList : ScriptableObject
{
    public GameObject[] obstaclePrefabs;
}
