﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleMovement : MonoBehaviour
{
    public float speed = 5f;
    [Range(0, 8)]
    public float amplitude = 2f;
    public float turnSpeedDegrees = 45;

    private float minX;
    private float maxX;
    private int moveDirection = 1;
    private Vector3 nextPoint;
    
    
    // Start is called before the first frame update
    void Start()
    {
        var collider = GetComponentInChildren<Obstacle>().GetComponent<Collider>();
        var absoluteMinX = -5 + collider.bounds.extents.x;
        var absoluteMaxX = 5 - collider.bounds.extents.x;
        minX = transform.position.x - amplitude / 2;
        maxX = transform.position.x + amplitude / 2;

        if (minX < absoluteMinX)
        {
            maxX += (absoluteMinX - minX);
            minX = absoluteMinX;
        }
        if (maxX > absoluteMaxX)
        {
            minX -= (maxX - absoluteMinX);
            maxX = absoluteMaxX;
        }
        minX = Mathf.Max(minX, absoluteMinX);
        maxX = Mathf.Min(maxX, absoluteMaxX);
        
        CalculateNextTurnPoint();
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x <= minX || transform.position.x >= maxX) {
            moveDirection *= -1;
            CalculateNextTurnPoint();
        }

        var newPosition = transform.position;
        newPosition.x += speed * moveDirection * Time.deltaTime;
        transform.position = newPosition;

        var lookDirection = nextPoint - transform.position;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(lookDirection), turnSpeedDegrees * Time.deltaTime);
    }

    private void CalculateNextTurnPoint()
    {
        nextPoint = transform.position;
        nextPoint.z -= speed;
        if (moveDirection > 0) {
            nextPoint.x = maxX;
        }
        else {
            nextPoint.x = minX;
        }
    }
}
