﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PersistentDataManager
{
    public static int PlayerLives {get; set;} = -1;
    public static int ObstaclesPassed { get; set; }
    public static float PlayerScore { get; set; } = -1;
    public static float TimePlayingSeconds { get; set; }

    public static void Reset() {
        PlayerLives = -1;
        ObstaclesPassed = 0;
        PlayerScore = -1;
        TimePlayingSeconds = 0;
    }
}
