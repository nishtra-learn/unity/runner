﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BgmPlayer : MonoBehaviour
{
    public UnityEvent startBgmAction;
 
    private void Start() {
        SoundManager.Instance.StopAllSoundSources();
        startBgmAction?.Invoke();
    }


    #region Start BGM
    public void StartMenuBgm()
    {
        var bgm = SoundManager.Instance.GetMenuBgm();
        if (!bgm.isPlaying)
        {
            bgm.Play();
        }
    }

    public void StartLevel1Bgm()
    {
        var bgm = SoundManager.Instance.GetLevel1Bgm();
        if (!bgm.isPlaying)
        {
            bgm.Play();
        }
    }

    public void StartLevel2Bgm()
    {
        var bgm = SoundManager.Instance.GetLevel2Bgm();
        if (!bgm.isPlaying)
        {
            bgm.Play();
        }
    }

    public void StartLevel3Bgm()
    {
        var bgm = SoundManager.Instance.GetLevel3Bgm();
        if (!bgm.isPlaying)
        {
            bgm.Play();
        }
    }
    #endregion
}
