﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Sound
{
    public string name;
    public AudioClip audioClip;
    [Range(0, 1f)]
    public float volume = 1f;
    [Range(-3f, 3f)]
    public float pitch = 1f;
    public bool loop;
    [Range(0, 1f)]
    public float spatialBlend;
    public AudioRolloffMode rolloffMode;
    [Min(0)]
    public float minDistance = 1f;
    [Min(0)]
    public float maxDistance = 50f;

    [HideInInspector]
    public AudioSource audioSource;
}
