﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerSndFx : MonoBehaviour
{
    public SoundBank soundFx;
    private Dictionary<string, AudioSource> soundMap = new Dictionary<string, AudioSource>();

    private void Awake()
    {
        foreach (var sound in soundFx.sounds)
        {
            sound.audioSource = gameObject.AddComponent<AudioSource>();
            sound.audioSource.clip = sound.audioClip;
            sound.audioSource.volume = sound.volume;
            sound.audioSource.pitch = sound.pitch;
            sound.audioSource.loop = sound.loop;
            sound.audioSource.spatialBlend = sound.spatialBlend;
            sound.audioSource.rolloffMode = sound.rolloffMode;
            sound.audioSource.minDistance = sound.minDistance;
            sound.audioSource.maxDistance = sound.maxDistance;
        }

        foreach (var feetCollider in GetComponentsInChildren<CharacterFeetCollider>())
        {
            feetCollider.OnGroundCollision += (collider) =>
            {
                PlayStepSound();
            };
        }

        var player = GetComponent<Player>();
        if (player)
        {
            player.OnHealthChange += (hp, delta) =>
            {
                if (delta < 0)
                    PlayTakenDamageSound();
            };
        }
    }

    public void PlayStepSound()
    {
        if (!soundMap.ContainsKey(nameof(PlayStepSound)))
        {
            var sound = soundFx.sounds.FirstOrDefault(s => s.name.ToLower() == "step");
            soundMap.Add(nameof(PlayStepSound), sound.audioSource);
        }
        soundMap[nameof(PlayStepSound)]?.Play();
    }

    public void PlayTakenDamageSound()
    {
        if (!soundMap.ContainsKey(nameof(PlayTakenDamageSound)))
        {
            var sound = soundFx.sounds.FirstOrDefault(s => s.name.ToLower() == "taken_damage");
            soundMap.Add(nameof(PlayTakenDamageSound), sound.audioSource);
        }
        soundMap[nameof(PlayTakenDamageSound)]?.Play();
    }

    public void PlayJumpSound()
    {
        if (!soundMap.ContainsKey(nameof(PlayJumpSound)))
        {
            var sound = soundFx.sounds.FirstOrDefault(s => s.name.ToLower() == "jump");
            soundMap.Add(nameof(PlayJumpSound), sound.audioSource);
        }
        soundMap[nameof(PlayJumpSound)]?.Play();
    }

}
