﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterFeetCollider : MonoBehaviour
{
    public event Action<Collider> OnGroundCollision;

    private void OnTriggerEnter(Collider other) {
        if (other.GetComponentInParent<GroundMarker>() != null) {
            OnGroundCollision?.Invoke(other);
        }
    }
}
