﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [SerializeField]
    private SoundBank[] soundBanks;

    public static SoundManager Instance { get; private set; }
    public bool IsMuted {get; private set;}

    private AudioListener audioListener;
    private Dictionary<string, AudioSource> soundMap = new Dictionary<string, AudioSource>();
    

    private void Awake()
    {
        // set up singleton
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        ////////

        audioListener = FindObjectOfType<AudioListener>();

        foreach (var bank in soundBanks)
        {
            foreach (var sound in bank.sounds)
            {
                sound.audioSource = gameObject.AddComponent<AudioSource>();
                sound.audioSource.clip = sound.audioClip;
                sound.audioSource.volume = sound.volume;
                sound.audioSource.pitch = sound.pitch;
                sound.audioSource.loop = sound.loop;
                sound.audioSource.spatialBlend = sound.spatialBlend;
                sound.audioSource.rolloffMode = sound.rolloffMode;
                sound.audioSource.minDistance = sound.minDistance;
                sound.audioSource.maxDistance = sound.maxDistance;

                soundMap.Add(sound.name, sound.audioSource);
            }
        }
    }

    #region Get BGM
    public AudioSource GetMenuBgm()
    {
        return soundMap["menu"];
    }

    public AudioSource GetLevel1Bgm()
    {
        return soundMap["level1"];
    }

    public AudioSource GetLevel2Bgm()
    {
        return soundMap["level2"];
    }

    public AudioSource GetLevel3Bgm()
    {
        return soundMap["level3"];
    }
    #endregion


    #region Start BGM
    public void StartMenuBgm()
    {
        var bgm = soundMap["menu"];
        if (!bgm.isPlaying)
        {
            bgm.Play();
        }
    }

    public void StartLevel1Bgm()
    {
        var bgm = soundMap["level1"];
        if (!bgm.isPlaying)
        {
            bgm.Play();
        }
    }

    public void StartLevel2Bgm()
    {
        var bgm = soundMap["level2"];
        if (!bgm.isPlaying)
        {
            bgm.Play();
        }
    }

    public void StartLevel3Bgm()
    {
        var bgm = soundMap["level3"];
        if (!bgm.isPlaying)
        {
            bgm.Play();
        }
    }
    #endregion


    #region Play SFX
    public void PlayPauseSfx()
    {
        soundMap["pause"].Play();
    }

    public void PlayUnpauseSfx()
    {
        soundMap["unpause"].Play();
    }

    public void PlayWinSfx()
    {
        soundMap["win"].Play();
    }

    public void PlayGameOverSfx()
    {
        soundMap["game_over"].Play();
    }

    public void PlayMuteBtnClickSfx() {
        soundMap["mute_btn_click"].Play();
    }
    #endregion


    public void StopAllSoundSources()
    {
        foreach (var sound in soundMap.Values)
        {
            sound?.Stop();
        }
    }

    public void MuteSound()
    {
        IsMuted = true;
        AudioListener.volume = 0;
    }

    public void UnmuteSound() {
        IsMuted = false;
        AudioListener.volume = 1f;
    }
}
