﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButtonSndFx : MonoBehaviour
{
    public new AudioSource audio;
    public AudioClip hoverSound;
    public AudioClip clickSound;

    public void SoundOnMouseHover() {
        audio.PlayOneShot(hoverSound);
    }

    public void SoundOnMouseClick() {
        audio.PlayOneShot(clickSound);
    }
}
