﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ScoreRecord
{
    public float score;
    public float timeSeconds;

    public ScoreRecord(float score, float time)
    {
        this.score = score;
        this.timeSeconds = time;
    }
    
}
