﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public static class HighscoreManager
{
    public static List<ScoreRecord> scores = new List<ScoreRecord>();

    private static string highscoresFilename = "highscores.json";

    static HighscoreManager() {
        LoadFromFile();
    }

    public static ScoreRecord GetTopScore() {
        return scores.FirstOrDefault();
    }
    
    public static void AddNewHighscore(ScoreRecord scoreRecord) {
        scores.Add(scoreRecord);
        scores.Sort((i1, i2) => {
            float diff = i1.score - i2.score;
            return diff < 0 ? 1 : -1;
        });
        scores = scores.Take(5).ToList();
        
        SaveToFile();
    }

    public static void SaveToFile() {
        var highscoreList = new HighscoreList();
        highscoreList.scores = scores;
        string json = JsonUtility.ToJson(highscoreList);
        File.WriteAllText(GetHighscoresFilePath(), json);
    }

    public static void LoadFromFile() {
        var filepath = GetHighscoresFilePath();
        if (!File.Exists(filepath))
            return;

        var json = File.ReadAllText(filepath);
        var highscoreList = JsonUtility.FromJson<HighscoreList>(json);
        scores = highscoreList.scores;
    }

    private static string GetHighscoresFilePath() {
        return Application.persistentDataPath + Path.DirectorySeparatorChar + highscoresFilename;
    }


    // a wrapper for json serialization
    private class HighscoreList {
        public List<ScoreRecord> scores;
    }
}
