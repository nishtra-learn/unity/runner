﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
GameManager a "scene-scoped" should be created once per scene.
Other instances created in the same scene will be destroyed.
Dosn't survive changing scenes.
*/
public class GameManager : MonoBehaviour
{
    [SerializeField] private Text livesUiText;
    [SerializeField] private Text timeUiText;
    [SerializeField] private Text obstaclesPassedUiText;
    [SerializeField] private Text scoreUiText;
    [SerializeField] private int scoreToWin = 1000;
    [SerializeField] private GameObject hudUi;
    [SerializeField] private GameObject levelCompleteMenu;
    [SerializeField] private GameObject gameOverMenu;

    private ObstacleManager obstacleManager;
    private Player player;
    private float levelScore;
    private bool hasWon = false;
    private bool hasLost = false;
    

    private void Start()
    {
        obstacleManager = gameObject.GetComponent<ObstacleManager>();
        player = FindObjectOfType<Player>();
        
        // subscribe to obstacle manager
        obstacleManager.OnObstaclePassed += () =>
        {
            PersistentDataManager.ObstaclesPassed++;
            obstaclesPassedUiText.text = PersistentDataManager.ObstaclesPassed.ToString();
        };
        if (PersistentDataManager.ObstaclesPassed > 0) {
            obstaclesPassedUiText.text = PersistentDataManager.ObstaclesPassed.ToString();
        }

        // subscribe to player
        livesUiText.text = player.HP.ToString();
        player.OnHealthChange += (hp, delta) => { 
            PersistentDataManager.PlayerLives = hp;
            livesUiText.text = hp.ToString(); 
        };
        player.OnScoreChange += (score, delta) =>
        {
            levelScore = score;
            PersistentDataManager.PlayerScore += delta;
            scoreUiText.text = score.ToString("0");
        };
        player.OnDeath += Lose;
        
        if (PersistentDataManager.PlayerLives != -1) {
            player.HP = PersistentDataManager.PlayerLives;
        }
    }

    private void Update()
    {
        if (!hasLost)
        {
            PersistentDataManager.TimePlayingSeconds += Time.deltaTime;
            float timePlaying = PersistentDataManager.TimePlayingSeconds;
            int minutes = Mathf.FloorToInt(timePlaying / 60);
            int seconds = Mathf.FloorToInt(timePlaying % 60);
            int ms = Mathf.FloorToInt((timePlaying - (int)timePlaying) * 100);
            timeUiText.text = $"{minutes}:{seconds:00}:{ms:000}";

            if (levelScore >= scoreToWin && !hasWon) {
                Win();
            }
        }
    }

    private void Win()
    {
        hasWon = true;
        var player = FindObjectOfType<Player>();
        player.PlayVictoryAnimation();
        player.OnAnimationEnd += OnPlayerAnimationEnd;
        
        HighscoreManager.AddNewHighscore(
            new ScoreRecord(
                PersistentDataManager.PlayerScore, 
                PersistentDataManager.TimePlayingSeconds));
        //ShowLevelCompleteMenuAfterDelay(1f);

        foreach (var audioSource in FindObjectsOfType<AudioSource>())
        {
            if (audioSource.gameObject.GetComponentInChildren<Obstacle>() != null) {
                audioSource.Stop();
            }
        }
        SoundManager.Instance.PlayWinSfx();
    }

    private void OnPlayerAnimationEnd(string animationName) {
        StartCoroutine(ShowMenuAfterDelay(levelCompleteMenu, 0));
        player.OnAnimationEnd -= OnPlayerAnimationEnd;
    }

    private void Lose()
    {
        hasLost = true;
        HighscoreManager.AddNewHighscore(
            new ScoreRecord(
                PersistentDataManager.PlayerScore, 
                PersistentDataManager.TimePlayingSeconds));
        ShowGameOverMenuAfterDelay(1f);

        foreach (var audioSource in FindObjectsOfType<AudioSource>())
        {
            audioSource.Stop();
        }
        SoundManager.Instance.StopAllSoundSources();
        SoundManager.Instance.PlayGameOverSfx();
    }

    private void ShowGameOverMenuAfterDelay(float seconds) {
        StartCoroutine(ShowMenuAfterDelay(gameOverMenu, seconds));
    }

    private IEnumerator ShowMenuAfterDelay(GameObject menuObject, float seconds) {
        yield return new WaitForSecondsRealtime(seconds);
        
        Time.timeScale = 0f;
        hudUi.SetActive(false);
        menuObject.SetActive(true);
    }


    
    #region Singleton setup
    public static GameManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion
}
