﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadManager : MonoBehaviour
{
    public GameObject roadSectionPrefab;
    public float roadSectionLength = 100;
    public GameObject player;
    public Camera mainCamera;
    public GameObject lightpostPrefab;
    
    private GameObject lastSpawnedSection;
    private Queue<GameObject> sections = new Queue<GameObject>();
    private Queue<GameObject> roadsideObjects = new Queue<GameObject>();
    

    private void Start()
    {
        lastSpawnedSection = GameObject.Find("Road Section");
        sections.Enqueue(lastSpawnedSection);
        SpawnLightposts();
    }



    private void Update()
    {
        bool playerIsPastTheStartOfRoadSection = player.transform.position.z > lastSpawnedSection.transform.position.z - lastSpawnedSection.transform.localScale.z / 2;
        if (playerIsPastTheStartOfRoadSection)
        {
            var nextSectionPosition = lastSpawnedSection.transform.position;
            nextSectionPosition.z = lastSpawnedSection.transform.position.z + lastSpawnedSection.transform.localScale.z / 2 + roadSectionLength / 2;
            lastSpawnedSection = Instantiate(roadSectionPrefab, nextSectionPosition, Quaternion.identity);
            lastSpawnedSection.transform.localScale = new Vector3(1, 1, roadSectionLength);
            sections.Enqueue(lastSpawnedSection);

            SpawnLightposts();
        }

        DestroyOutOfViewSection();
        DestroyOutOFViewObjects();
    }

    private void SpawnLightposts()
    {
        var sectionStart = lastSpawnedSection.transform.position.z - lastSpawnedSection.transform.localScale.z / 2;
        var sectionEnd = lastSpawnedSection.transform.position.z + lastSpawnedSection.transform.localScale.z / 2;
        for (int i = (int)sectionStart; i < (int)sectionEnd; i++)
        {
            if (i % 50 == 0)
            {
                var position = new Vector3(6, 0, i);
                var lightpostRight = Instantiate(lightpostPrefab, position, Quaternion.identity);
                roadsideObjects.Enqueue(lightpostRight);
                position.x *= -1;
                var lightpostLeft = Instantiate(lightpostPrefab, position, Quaternion.Euler(0, 180, 0));
                roadsideObjects.Enqueue(lightpostLeft);
            }
        }
    }

    private void DestroyOutOfViewSection()
    {
        var section = sections.Peek();
        var furthestPoint = section.transform.position + section.transform.localScale / 2;
        if (!Helpers.IsPointInView(furthestPoint, mainCamera)) {
            Destroy(sections.Dequeue());
        }
    }

    private void DestroyOutOFViewObjects() {
        while (roadsideObjects.Count > 0) {
            var obj = roadsideObjects.Peek();
            if (!Helpers.IsPointInView(obj.transform.position, mainCamera)) {
                Destroy(roadsideObjects.Dequeue(), 1);
            }
            else {
                break;
            }
        }
    }
}
