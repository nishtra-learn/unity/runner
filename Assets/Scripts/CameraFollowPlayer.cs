﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowPlayer : MonoBehaviour
{
    public Vector3 offset = new Vector3(0, 1.5f, -2.5f);
    public GameObject player;

    private void LateUpdate() {
        transform.position = player.transform.position + offset;
    }
}
