﻿using UnityEngine;

public class Helpers
{
    public static bool IsPointInView(Vector3 point, Camera camera)
    {
        Vector3 screenPoint = camera.WorldToViewportPoint(point);
        if (screenPoint.z > 0
            && screenPoint.x > 0 && screenPoint.x < 1
            && screenPoint.y > 0 && screenPoint.y < 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
