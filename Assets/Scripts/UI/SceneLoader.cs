﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


/**
Provides a "scene-scoped" singleton to allow static access to scene loading with crossfade. 
Meaning that every new instance overwrites the existing one.
*/
public class SceneLoader : MonoBehaviour
{
    public Animator animator;
    public float transitionTime = 1f;


    // set up scene-scoped singleton
    public static SceneLoader Instance { get; private set; }

    private void Awake() {
        if (Instance != null) {
            Debug.LogWarning("An existing instance of SceneLoader was overwritten");
        }
        Instance = this;
    }
    /////////////////////////////////


    public void LoadScene(int sceneIndex) {
        StartCoroutine(FadeToScene(sceneIndex));
    }

    private IEnumerator FadeToScene(int sceneIndex) {
        animator.SetTrigger("FadeOut");
        yield return new WaitForSecondsRealtime(transitionTime);
        SceneManager.LoadScene(sceneIndex);
        Time.timeScale = 1f;
    }
}
