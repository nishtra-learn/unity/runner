﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    public GameObject pauseUi;
    public Text currentScore;
    public Text bestScore;

    public bool GameIsPaused { get; private set; } = false;

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (GameIsPaused) {
                Resume();
            }
            else {
                Pause();
            }
        }

        currentScore.text = PersistentDataManager.PlayerScore.ToString("0");
        var bestScoreRecord = HighscoreManager.GetTopScore();
        bestScore.text = bestScoreRecord.score.ToString("0");
    }

    private void Pause() {
        pauseUi.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;

        SoundManager.Instance.PlayPauseSfx();
    }

    private void Resume() {
        pauseUi.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;

        SoundManager.Instance.PlayUnpauseSfx();
    }

    public void OnBtnResumeClick() {
        Resume();
    }

    public void OnBtnReturnToMenuClick() {
        HighscoreManager.AddNewHighscore(
            new ScoreRecord(
                PersistentDataManager.PlayerScore, 
                PersistentDataManager.TimePlayingSeconds));
        SceneLoader.Instance.LoadScene(Scenes.MainMenu);
    }
}
