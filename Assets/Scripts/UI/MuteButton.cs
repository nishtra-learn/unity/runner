﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MuteButton : MonoBehaviour
{
    [SerializeField]
    private Sprite muteButtonSpriteDefault;
    [SerializeField]
    private Sprite muteButtonSpriteMuted;

    private Image muteButtonImage;


    private void Awake() {
        muteButtonImage = GetComponent<Image>();
    }


    public void OnClick()
    {
        StartCoroutine(MuteBtnClickBehaviour(0.18f));
    }

    private IEnumerator MuteBtnClickBehaviour(float delayedBySeconds) {
        // plays sfx on mute
        if (!SoundManager.Instance.IsMuted) {
            SoundManager.Instance.PlayMuteBtnClickSfx();
        }

        yield return new WaitForSecondsRealtime(delayedBySeconds);
        
        if (SoundManager.Instance.IsMuted)
        {
            SoundManager.Instance.UnmuteSound();
            muteButtonImage.sprite = muteButtonSpriteDefault;
        }
        else
        {
            SoundManager.Instance.MuteSound();
            muteButtonImage.sprite = muteButtonSpriteMuted;
        }

        // plays sfx on unmute
        if (!SoundManager.Instance.IsMuted) {
            SoundManager.Instance.PlayMuteBtnClickSfx();
        }
    }
}
