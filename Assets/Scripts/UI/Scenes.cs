﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Scenes
{
    public static int MainMenu { get; private set; } = 0;
    public static int Highscores { get; private set; } = 1;
    
    
    public static List<int> Levels { get; private set; } = new List<int> {
        2, 3, 4
    };
}
