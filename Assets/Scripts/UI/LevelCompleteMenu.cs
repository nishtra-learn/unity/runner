﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelCompleteMenu : MonoBehaviour
{
    public GameObject nextLevelBtn;
    public Text currentScore;
    public Text bestScore;
    private int nextLevelSequenceIdx = -1;


    private void Update() {
        currentScore.text = PersistentDataManager.PlayerScore.ToString("0");
        var bestScoreRecord = HighscoreManager.GetTopScore();
        bestScore.text = bestScoreRecord.score.ToString("0");
    }

    private void Awake() {
        int currentLevelBuildIndex = SceneManager.GetActiveScene().buildIndex;
        int currentLevelSequenceIdx = Scenes.Levels.FindIndex(lvlBuildIdx => lvlBuildIdx == currentLevelBuildIndex);
        if (currentLevelSequenceIdx + 1 < Scenes.Levels.Count) {
            nextLevelSequenceIdx = currentLevelSequenceIdx + 1;
        }
        else {
            nextLevelBtn.SetActive(false);
        }
    }

    public void OnBtnNextLevelClick() {
        SceneLoader.Instance.LoadScene(Scenes.Levels[nextLevelSequenceIdx]);
    }

    public void OnBtnReturnToMenuClick() {
        SceneLoader.Instance.LoadScene(Scenes.MainMenu);
    }
}
