﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighscoresMenu : MonoBehaviour
{
    public GameObject scoreRecordTemplate;
    public GameObject container;

    private List<ScoreRecord> scores;

    private void Awake() {
        GetScores();

        for (int i = 0; i < scores.Count; i++)
        {
            var scoreRecordObj = Instantiate(scoreRecordTemplate, container.transform);
            var pos = scoreRecordObj.transform.Find("Pos").GetComponent<Text>();
            var score = scoreRecordObj.transform.Find("Score").GetComponent<Text>();
            var time = scoreRecordObj.transform.Find("Time").GetComponent<Text>();
            var background = scoreRecordObj.transform.Find("Background").GetComponent<Image>();

            pos.text = $"{i + 1}";
            score.text = scores[i].score.ToString("0");
            var timePlaying = scores[i].timeSeconds;
            int minutes = Mathf.FloorToInt(timePlaying / 60);
            int seconds = Mathf.FloorToInt(timePlaying % 60);
            int ms = Mathf.FloorToInt((timePlaying - (int)timePlaying) * 100);
            time.text = $"{minutes}:{seconds:00}:{ms:000}";

            background.color = new Color();
            var col = background.color;
            col.a = i % 2 == 0 ? 0.2f : 0.1f;
            background.color = col;
        }
    }

    private void GetScores() {
        HighscoreManager.LoadFromFile();
        scores = HighscoreManager.scores;
    }

    public void OnBackBtnClick() {
        SceneLoader.Instance.LoadScene(Scenes.MainMenu);
    }
}
