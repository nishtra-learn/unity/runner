﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverMenu : MonoBehaviour
{
    public Text currentScore;
    public Text bestScore;


    private void Update() {
        currentScore.text = PersistentDataManager.PlayerScore.ToString("0");
        var bestScoreRecord = HighscoreManager.GetTopScore();
        bestScore.text = bestScoreRecord.score.ToString("0");
    }
    
    public void OnBtnReturnToMenuClick() {
        SceneLoader.Instance.LoadScene(Scenes.MainMenu);
    }
}
