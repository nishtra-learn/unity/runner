﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    public void OnStartBtnClick()
    {
        PersistentDataManager.Reset();
        SceneLoader.Instance.LoadScene(Scenes.Levels[0]);
    }

    
    public void OnExitBtnClick()
    {
        Debug.Log("Game exit");
        Application.Quit();
    }

    public void OnHighscoresBtnClick()
    {
        SceneLoader.Instance.LoadScene(Scenes.Highscores);
    }
}
