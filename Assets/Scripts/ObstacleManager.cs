﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ObstacleManager : MonoBehaviour
{
    //public GameObject[] obstaclePrefabs;
    public ObstacleList obstacleList;
    public float intervalSeconds = 2f;
    public float obstacleGrazeDistance = 1f;
    public Camera mainCamera;

    public bool isActive = true;
    public event Action OnObstaclePassed;
    
    
    private void Start() {
        StartCoroutine(Timer());
    }

    private IEnumerator Timer() {
        while (true) {
            yield return new WaitForSeconds(intervalSeconds);
            
            if (!isActive)
                continue;

            var obstaclePrefabs = obstacleList.obstaclePrefabs;
            var randomObstaclePrefab = obstaclePrefabs[UnityEngine.Random.Range(0, obstaclePrefabs.Length)];
            
            var obj = Instantiate(randomObstaclePrefab, Vector3.zero, Quaternion.identity);
            var obstacle = obj.GetComponentInChildren<Obstacle>();
            obstacle.mainCamera = mainCamera;
            obstacle.OnPassedWithoutCollision += () => {
                OnObstaclePassed?.Invoke();
            };
            var grazeColliderMarker = obj.GetComponentInChildren<ObstacleGrazeCollider>();
                        
            // set scale
            float scaleMult = 1;
            if (obj.GetComponent<ObstacleScalable>() != null) {
                scaleMult = UnityEngine.Random.Range(1f, 3f);
                obj.transform.localScale *= scaleMult;
            }

            // set position
            var minX = -5 + scaleMult / 2 + 0.1f;
            var maxX = 5 - scaleMult / 2 - 0.1f;
            var grazeHeight = grazeColliderMarker.GetComponent<Collider>().bounds.size.y * scaleMult;
            var position = new Vector3(
                UnityEngine.Random.Range(minX, maxX),
                grazeHeight / 2,
                mainCamera.transform.position.z + 100
            );
            ObstacleRocket rocket = obj.GetComponent<ObstacleRocket>();
            if (rocket != null) {
                position.y += rocket.distanceToGround;
            }
            obj.transform.position = position;
            
            
            // set rotation
            obj.transform.Rotate(Vector3.up, 180, Space.World);

            // set up graze collider
            var parentTransform = grazeColliderMarker.transform.parent;
            grazeColliderMarker.transform.parent = null;
            grazeColliderMarker.transform.localScale = grazeColliderMarker.transform.localScale + Vector3.one * obstacleGrazeDistance;
            grazeColliderMarker.transform.parent = parentTransform;
        }
    }
}
